/*
 * tinysh.c
 *
 * Minimal portable shell
 *
 * Copyright (C) 2001 Michel Gutierrez <mig@nerim.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "tinysh.h"
#include "stdio.h"
#include "main.h"

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 	256
#endif
#ifndef HISTORY_DEPTH
#define HISTORY_DEPTH 	3		// 16
#endif
#ifndef MAX_ARGS
#define MAX_ARGS 		11		// 16
#endif
#ifndef PROMPT_SIZE
#define PROMPT_SIZE 	10 		// 16
#endif
#ifndef TOPCHAR
#define TOPCHAR 		'/'
#endif

typedef unsigned char uchar;

/* redefine some useful and maybe missing utilities to avoid conflicts */
#define	strlen	tinysh_strlen
#define	puts	tinysh_puts

static void help_fnt(int argc, char **argv);
static tinysh_cmd_t help_cmd={  0,"help","	   	[no argument]","[no argument]",help_fnt,0,0,0 };
static int strlen(uchar *s);
static void puts(char *s);

static uchar 		input_buffers[HISTORY_DEPTH][BUFFER_SIZE+1] = {0};
static uchar 		trash_buffer[BUFFER_SIZE+1] ={0};
static int 	 		cur_buf_index=0;
static uchar 		context_buffer[BUFFER_SIZE+1] ={0};
static int 	 		cur_context=0;
static int 	 		cur_index=0;
static int 	 		echo=1;
static char  		prompt[PROMPT_SIZE+1]="$ ";
static tinysh_cmd_t *root_cmd=&help_cmd;
static tinysh_cmd_t *cur_cmd_ctx=0;
static void 		*tinysh_arg=0;


extern CAN_TxHeaderTypeDef   TxHeader;
extern uint8_t               TxData[8];
extern uint8_t buttonFlag;
extern UART_HandleTypeDef huart2;
extern uint8_t timerFlag;
extern uint8_t timerSet;


extern void RTC_CalendarConfig(RTC_DateTypeDef *date,RTC_TimeTypeDef *time);

/************************************
 * @brief To insert STANDART CAN ID *
 ************************************/
static void canIDstdFunc(int argc, char **argv)
{
	//printf("%d CAN ID std func, %s %s %s\r\n",argc,*argv,argv[1],argv[2]);
	if (argc<2){
		printf("argument too short\r\n");
		return;
	}
	
	if (argc>10){
		printf("argument too long\r\n");
		return;
	}
	
	unsigned long id = tinysh_atoxi(argv[1]);
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.StdId = (uint32_t)id;
	TxHeader.DLC = argc - 2;
	uint8_t i,j=0;
	for(i=2;i<=argc;i++){
		unsigned long candata = tinysh_atoxi(argv[i]);
		TxData[j] = (uint8_t)candata;
		j++;
	}
}

/************************************
 * @brief To insert EXTENDED CAN ID *
 ************************************/
static void canIDextFunc(int argc, char **argv)
{
	if (argc<2){
		printf("argument too short\r\n");
		return;
	}
	
	if (argc>10){
		printf("argument too long\r\n");
		return;
	}
	
	unsigned long id = tinysh_atoxi(argv[1]);
	TxHeader.IDE = CAN_ID_EXT;
	TxHeader.ExtId = (uint32_t)id;
	TxHeader.DLC = argc - 2;
	uint8_t i,j=0;
	for(i=2;i<=argc;i++){
		unsigned long candata = tinysh_atoxi(argv[i]);
		TxData[j] = (uint8_t)candata;
		j++;
	}
}

/**********************************
 * @brief To Change CAN1 Setting  *
 **********************************/
static void cansetupFunc(int argc, char **argv)
{
	if (argc<2){
		printf("argument too short\r\n");
		return;
	}
	
	if (argc>2){
		printf("argument too long\r\n");
		return;
	}
	
	unsigned long cansetting = tinysh_atoxi(argv[1]);
	MX_CAN1_Init((t_canInitSetting)cansetting);
}


/**********************************
 * @brief To Change Button Settin *
 **********************************/
static void buttonsetupFunc(int argc, char **argv)
{
	if (argc<2){
		printf("argument too short\r\n");
		return;
	}
	
	if (argc>2){
		printf("argument too long\r\n");
		return;
	}
	
	uint8_t buttonsetting = (uint8_t)tinysh_atoxi(argv[1]);
	
	if(buttonsetting > 3){
		printf("max value is 3\r\n");
		return;
	}
	
	buttonFlag &= ~(1UL << CAN_REQ);
	buttonFlag &= ~(1UL << SPIM_REQ);
	buttonFlag &= ~(1UL << I2CM_REQ);
	buttonFlag &= ~(1UL << ADC_REQ);
	
	if(buttonsetting == 0)
		buttonFlag |= 1UL << CAN_REQ;
	else if(buttonsetting == 1)
		buttonFlag |= 1UL << SPIM_REQ;
	else if(buttonsetting == 2)
		buttonFlag |= 1UL << I2CM_REQ;
	else if(buttonsetting == 3)
		buttonFlag |= 1UL << ADC_REQ;
}	


/**********************************
 * @brief To Write/Read Thru SPI3 *
 **********************************/
static void spimsetupFunc(int argc, char **argv)
{
	if (argc<2){
		printf("argument too short\r\n");
		return;
	}
	
	
	uint8_t spidata[SPIBUFFER];
	uint16_t spicount = (uint16_t)tinysh_atoxi(argv[1]);
	
	if (spicount>SPIBUFFER){
		printf("max length is %d\r\n",I2CBUFFER);
		return;
	}
	
	uint8_t i;
	
	for(i=0;i<spicount;i++){
		spidata[i] = (uint8_t)tinysh_atoxi(argv[i+2]);
	}
	
	spi3_transmit_receive((uint8_t*)&spidata,spicount);
}

/**********************************
 * @brief To Write/Read Thru SPI1 *
 **********************************/
static void spissetupFunc(int argc, char **argv)
{
	if (argc<2){
		printf("argument too short\r\n");
		return;
	}
	
	
	uint8_t spidata[SPIBUFFER];
	uint16_t spicount = (uint16_t)tinysh_atoxi(argv[1]);
	
	if (spicount>SPIBUFFER){
		printf("max length is %d\r\n",I2CBUFFER);
		return;
	}
	
	uint8_t i;
	
	for(i=0;i<spicount;i++){
		spidata[i] = (uint8_t)tinysh_atoxi(argv[i+2]);
	}
	
	spi1_transmit_receive((uint8_t*)&spidata,spicount);
}


/****************************************
 * @brief To Write/Read Thru I2C Master *
 ****************************************/
static void i2cmsetupFunc(int argc, char **argv)
{
	if (argc<3){
		printf("argument too short\r\n");
		return;
	}
	
	if (argc>8){
		printf("argument too long\r\n");
		return;
	}
	
	uint8_t i2cdata[I2CBUFFER];
	uint16_t i2caddr = (uint16_t)tinysh_atoxi(argv[1]);
	uint8_t count = (uint16_t)tinysh_atoxi(argv[2]);
	
	if (count>I2CBUFFER){
		printf("max length is %d\r\n",I2CBUFFER);
		return;
	}
	
	
	uint8_t i;
	for(i=0;i<count;i++){
		i2cdata[i] = (uint8_t)tinysh_atoxi(argv[i+3]);
	}
	
	i2c1_transmit(i2cdata,count,i2caddr);
}

/***************************************
 * @brief To Write/Read Thru I2C Slave *
 ***************************************/
static void i2cssetupFunc(int argc, char **argv)
{
	if (argc<2){
		printf("argument too short\r\n");
		return;
	}
	
	if (argc>7){
		printf("argument too long\r\n");
		return;
	}
	
	uint8_t i2cdata[I2CBUFFER];
	uint8_t count = (uint16_t)tinysh_atoxi(argv[1]);
	
	if (count>I2CBUFFER){
		printf("max length is %d\r\n",I2CBUFFER);
		return;
	}
	
	uint8_t i;
	for(i=0;i<count;i++){
		i2cdata[i] = (uint8_t)tinysh_atoxi(argv[i+2]);
	}
	
	i2c2_transmit(i2cdata,count);
}

/***************************************
 * @brief To Write Thru USART2         *
 ***************************************/
static void uart2Func(int argc, char **argv)
{
	uint8_t uartTX[100];
	uint8_t i,j,data=0,len;
	
	for(i=1;i<argc;i++){
		for(j=0;j<50;j++){
			if(argv[i][j] == '\0'){
				memcpy(&uartTX[data],argv[i],len);
				data += len;
				memcpy(&uartTX[data]," ",1);
				data += 1;
				len = 0;
				break;
			}
			len++;
		}
	}
	
	data += 2;
	memcpy(&uartTX[data-2],"\n\r",2);
	
	HAL_UART_Transmit(&huart2,uartTX,data,10);
}

/***************************************
 * @brief To set timer 3               *
 ***************************************/
static void tim3Func(int argc, char **argv)
{
	timerFlag = (uint8_t)tinysh_atoxi(argv[1]);

	if (argc>2){
		timerSet = (uint8_t)tinysh_atoxi(argv[2]);
	}
}


/***************************************
 * @brief Update RTC                   *
 ***************************************/
static void rtcFunc(int argc, char **argv)
{
	RTC_DateTypeDef date;
	RTC_TimeTypeDef time;
	
	if (argc<5){
		printf("argument too short\r\n");
		return;
	}
	
	
	uint8_t Date  = (uint8_t)tinysh_atoxi(argv[1]);
	if(Date > 10){
		if(Date > 25){
			Date += 12;
		}else if(Date > 31){
			printf("max date is 31\r\n");
			return;
		}else{
			Date += 6;
		}
	}
	
	uint8_t month = (uint8_t)tinysh_atoxi(argv[2]);
	if(month > 10){
		if(month > 12){
			printf("max month is 12\r\n");
			return;
		}
		month += 6;
	}
	
	uint8_t year = (uint8_t)tinysh_atoxi(argv[3]);
	if(year > 10){
		year += 6;
	}
	
	date.Date  = Date;
	date.Month = month;
	date.Year  = year;
	date.WeekDay = RTC_WEEKDAY_FRIDAY;
	
	time.Hours		= (uint8_t)tinysh_atoxi(argv[4]);
	
	if(time.Hours > 15){
		time.Hours += 6;
	}else if(time.Hours > 23){
		printf("max hour is 23\r\n");
		return;
	}
	
	uint8_t minute	 	= (uint8_t)tinysh_atoxi(argv[5]);
	
	if(minute > 59){
		printf("max minutes / secon are 59\r\n");
		return;
	}else if(minute > 50){
		minute += 30;
	}else if(minute > 40){
		minute += 24;
	}else if(minute > 30){
		minute += 18;
	}else if(minute > 25){
		minute += 12;
	}else if(minute > 10){
		minute += 6;
	}
	
	time.Minutes 	= minute;
	time.Seconds 	= 0x00;
	
	time.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	time.StoreOperation = RTC_STOREOPERATION_RESET;
	RTC_CalendarConfig(&date,&time);
}

static tinysh_cmd_t canidstd={0,"canIDstd","		Insert STD CAN ID followed by CAN DATA","[ID] [DATA0] [DATA1] [DATA2] [DATA3] [DATA4] [DATA5] [DATA6] [DATA7]",canIDstdFunc,0,0,0};
static tinysh_cmd_t canidext={0,"canIDext","		Insert EXT CAN ID followed by CAN DATA","[ID] [DATA0] [DATA1] [DATA2] [DATA3] [DATA4] [DATA5] [DATA6] [DATA7]",canIDextFunc,0,0,0};
static tinysh_cmd_t cansetup={0,"cansetup","		Change CAN operation, LOOPBACK_250 or 500,NORMAL_250 or 500","[0/1/2/3]",cansetupFunc,0,0,0};
static tinysh_cmd_t buttonsetup={0,"buttonsetup","		Change user button operation for CAN/SPIM/I2CM/ADC","[0/1/2/3]",buttonsetupFunc,0,0,0};
static tinysh_cmd_t spimsetup={0,"spimsetup","		Insert SPI3 Data length and value","[LENGTH] [DATA0] [DATA1] [DATA2] [DATA3] [DATA4]",spimsetupFunc,0,0,0};
static tinysh_cmd_t spissetup={0,"spissetup","		Insert SPI1 Data length and value","[LENGTH] [DATA0] [DATA1] [DATA2] [DATA3] [DATA4]",spissetupFunc,0,0,0};
static tinysh_cmd_t i2cssetup={0,"i2cssetup","		Insert I2C Data to be transmitted to I2C Master","[DATA0] [DATA1]",i2cssetupFunc,0,0,0};
static tinysh_cmd_t i2cmsetup={0,"i2cmsetup","		Insert I2C Length, Addr & Value to be transmitted to I2C Slave","[I2C ADDR] [LENGTH] [DATA0] [DATA1] [DATA2] [DATA3] [DATA4]",i2cmsetupFunc,0,0,0};
static tinysh_cmd_t uart2setup={0,"uart2func","		Insert String","[STRING]",uart2Func,0,0,0};
static tinysh_cmd_t tim3setup={0,"timersetup","		OFF/ON/CUSTOM","[0/1/2] [DURATION]",tim3Func,0,0,0};
static tinysh_cmd_t rtcsetup={0,"rtcsetup","		Insert Date & Time","[DATE] [MONTH] [20XX] [HOUR] [MINUTE] [SECOND]",rtcFunc,0,0,0};


void tinysh_init(void)
{
	tinysh_set_prompt("\r\n\nPCI$");
	tinysh_add_command(&canidstd);				// Change CAN STD ID.
	tinysh_add_command(&canidext);				// Change CAN EXT ID.
	tinysh_add_command(&cansetup);				// Change CAN Setup.
	tinysh_add_command(&spimsetup);				// Read/Write Thru SPI3 MASTER.
	tinysh_add_command(&spissetup);				// Read/Write Thru SPI1 SLAVE.
	tinysh_add_command(&i2cssetup);				// Write setup for I2C2 SLAVE.
	tinysh_add_command(&i2cmsetup);				// Write setup for I2C1 MASTER.
	tinysh_add_command(&buttonsetup);			// Change Button Setup.
	tinysh_add_command(&uart2setup);			// Sent string to uart2.
	tinysh_add_command(&tim3setup);				// Set timer 3.
	tinysh_add_command(&rtcsetup);				// Set date & time.
}



/************************************************** 
 * few useful utilities that may be missing 
 **************************************************/
static int strlen(uchar *s)	{
	int i;
	for(i=0;*s;s++,i++);
	return i;
}

static void puts(char *s)
{
	while(*s){
		TM_USBD_CDC_Putc(TM_USB_FS,*s++);
	}
}


/***********************************************
 * Callback for help function 
 ***********************************************/
static void help_fnt(int argc, char **argv)
{
  puts("\r\n");
  puts("?  		Help command.\r\n");
  puts("CTRL-P  	Recall previous input line.\r\n");
  puts("CTRL-N		Next in history.\r\n");
  puts("CTRL-ENTER  	Force new line.\r\n");
  puts("TAB		Auto complete command.\r\n\n");
  puts("Commands List:\r\n");
  puts("canIDstd	[ID] [DATA0] to [DATA7]\r\n");
  puts("canIDext	[ID] [DATA0] to [DATA7]\r\n");
  puts("cansetup	[0/1/2/3] DESC:LOOPBACK_250/500,NORMAL_250/500\r\n");
  puts("buttonsetup	[0/1/2/3]	  DESC:CAN/SPIM/I2CM/ADC\r\n");
  puts("spimsetup	[LENGTH] [DATA0] to [DATA4]\r\n");
  puts("spissetup	[LENGTH] [DATA0] to [DATA4]\r\n");
  puts("i2cmsetup	[LENGTH] [I2C ADDR] [DATA0] to [DATA4]\r\n");
  puts("i2cssetup	[LENGTH] [DATA0] to [DATA4]\r\n");
  puts("uart2sent	[STRING]\r\n");	
  puts("timerset	[0/1/2] [DURATION]\r\n");
  puts("\r\n");
}


enum { NULLMATCH,FULLMATCH,PARTMATCH,UNMATCH,MATCH,AMBIG };

/********************************************************************************
 * verify if the non-spaced part of s2 is included at the begining of s1.
 * return FULLMATCH if s2 equal to s1, PARTMATCH if s1 starts with s2
 * but there are remaining chars in s1, UNMATCH if s1 does not start with s2
 ********************************************************************************/
int strstart(uchar *s1, uchar *s2)
{
  while(*s1 && *s1==*s2) { s1++; s2++; }

  if(*s2==' ' || *s2==0){
    if(*s1==0){
      return FULLMATCH; 	// full match.
    }else{
      return PARTMATCH; 	// partial match.
	}
  }else{
    return UNMATCH;     	// no match.
  }
}

/**********************************************************************
 * check commands at given level with input string.
 * _cmd: point to first command at this level, return matched cmd
 * _str: point to current unprocessed input, return next unprocessed
 **********************************************************************/
static int parse_command(tinysh_cmd_t **_cmd, uchar **_str)
{
  uchar *str=*_str;
  tinysh_cmd_t *cmd;
  tinysh_cmd_t *matched_cmd=0;

  /* first eliminate first blanks */
  while(*str==' ') str++;
  if(!*str){
	*_str=str;
	return NULLMATCH; 					// end of input.
  }

  /* first pass: count matches */
  for(cmd=*_cmd;cmd;cmd=cmd->next){
      int ret=strstart((uchar*)cmd->name,str);

      if(ret==FULLMATCH){
          /* found full match */
          while(*str && *str!=' ') str++;
          while(*str==' ') str++;
          *_str=str;
          *_cmd=cmd;
          return MATCH;
      
	  }else if (ret==PARTMATCH){
          if(matched_cmd){
              *_cmd=matched_cmd;
              return AMBIG;
          }else{
              matched_cmd=cmd;
          }
     }else{ 						// UNMATCH */
        
     }
  }
  
  if(matched_cmd){
      while(*str && *str!=' ') str++;
      while(*str==' ') str++;
      *_cmd=matched_cmd;
      *_str=str;
      return MATCH;
  }else{
    return UNMATCH;
  }
}

/********************************************************
 * create a context from current input line
 ********************************************************/
static void do_context(tinysh_cmd_t *cmd, uchar *str)
{
  while(*str)
    context_buffer[cur_context++]=*str++;
  context_buffer[cur_context]=0;
  cur_cmd_ctx=cmd;
}

/**********************************************************
 * execute the given command by calling callback 
 * with appropriate arguments
 **********************************************************/
static void exec_command(tinysh_cmd_t *cmd, uchar *str)
{
  char *argv[MAX_ARGS];
  int argc=0;
  int i;

  /* copy command line to preserve it for history */
  for(i=0;i<BUFFER_SIZE;i++){
    trash_buffer[i]=str[i];
  }
  
  str=trash_buffer;

  /* cut into arguments */
  argv[argc++]=cmd->name;
  while(*str && argc<MAX_ARGS){
      while(*str==' ') str++;
      
	  if(*str==0)
        break;
      
	  argv[argc++]=(char*)str;
      while(*str!=' ' && *str) str++;
      if(!*str) break;
      *str++=0;
  }
	
  /* call command function if present */
  if(cmd->function){
      tinysh_arg=cmd->arg;
      cmd->function(argc,&argv[0]);
  }
}

/***********************************************
 * try to execute the current command line
 ***********************************************/
static int exec_command_line(tinysh_cmd_t *cmd, uchar *_str)
{
  uchar *str=_str;

  while(1){
	int ret;
	ret=parse_command(&cmd,&str);
	
	if(ret==MATCH){ 						// found unique match.
		if(cmd){
			if(!cmd->child){ 				// no sub-command, execute.
               puts("\r\n");
               exec_command(cmd,str);
               return 0;
            
			}else{
				
               if(*str==0){ 				// no more input, this is a context.
					do_context(cmd,_str);
                    return 0;
               }else 						// process next command word.
                    cmd=cmd->child;
            }
        }else{ 								// cmd == 0
              puts("\r\n");
              return 0;
        }
    
	}else if(ret==AMBIG){
        puts("AMBIGUITY: ");
        puts((char*)str);
        puts("\r\n");
        return 0;
    
	}else if(ret==UNMATCH){ 				// UNMATCH.
		puts("\r\n  no match: ");
		puts((char*)str);
		puts("\r\n");
		return 0;
    
	}else{ 									// NULLMATCH.
        return 0;
    }
	
  }
}

/****************************************************
 * Display help for list of commands
 ****************************************************/
static void display_child_help(tinysh_cmd_t *cmd)
{
  tinysh_cmd_t *cm;
  int len=0;

  putchar('\n');
  for(cm=cmd;cm;cm=cm->next)
    if(len<strlen((uchar*)cm->name))
      len=strlen((uchar*)cm->name);
  for(cm=cmd;cm;cm=cm->next)
    if(cm->help){
        int i;
        puts(cm->name);
        for(i=strlen((uchar*)cm->name);i<len+2;i++)
          putchar(' ');
        puts(cm->help);
        puts("\r\n");
    }
}

/*******************************************************
 * Try to display help for current comand line
 *******************************************************/
static int help_command_line(tinysh_cmd_t *cmd, uchar *_str)
{
  uchar *str=_str;

  while(1){
	int ret;
    ret=parse_command(&cmd,&str);
    
	if(ret==MATCH && *str==0){ 				// found unique match or empty line.
       if(cmd->child){ 						// display sub-commands help.
          display_child_help(cmd->child);
          return 0;
        }else{  							// no sub-command, show single help.
           if(*(str-1)!=' ')
				putchar(' ');
           if(cmd->usage)
                puts(cmd->usage);
           puts(": ");
           
		   if(cmd->help)
                puts(cmd->help);
           else
                puts("no help available");
            putchar('\n');
         }
         return 0;
     
	 }else if(ret==MATCH && *str){			// continue processing the line.
          cmd=cmd->child;
     
	 }else if(ret==AMBIG){
          puts("\r\nambiguity: ");
          puts((char*)str);
          puts("\r\n");
          return 0;
     
	 }else if(ret==UNMATCH){
          puts("\r\nno match: ");
          puts((char*)str);
          puts("\r\n");
          return 0;
        
	 }else{ 							// NULLMATCH */
          if(cur_cmd_ctx)
            display_child_help(cur_cmd_ctx->child);
          else
            display_child_help(root_cmd);
          return 0;
     }
   }
}

/**************************************************************
 * try to complete current command line
 **************************************************************/
static int complete_command_line(tinysh_cmd_t *cmd, uchar *_str)
{
  uchar *str=_str;

  while(1)
  {
      int ret;
      int common_len=BUFFER_SIZE;
      int _str_len;
      int i;
      uchar *__str=str;

      ret=parse_command(&cmd,&str);
      for(_str_len=0;__str[_str_len]&&__str[_str_len]!=' ';_str_len++);
      if(ret==MATCH && *str){
          cmd=cmd->child;

  	  }else if(ret==AMBIG || ret==MATCH || ret==NULLMATCH){
          tinysh_cmd_t *cm;
          tinysh_cmd_t *matched_cmd=0;
          int nb_match=0;

          for(cm=cmd;cm;cm=cm->next){
              int r=strstart((uchar*)cm->name,__str);
              
			  if(r==FULLMATCH){
                  for(i=_str_len;cmd->name[i];i++)
                    tinysh_char_in(cmd->name[i]);
                  
				  if(*(str-1)!=' ')
                    tinysh_char_in(' ');
                  
				  if(!cmd->child){
                      if(cmd->usage){
                          puts(cmd->usage);
                          puts("\r\n");
                          return 1;
                      }else
                        return 0;
				  }else{
                      cmd=cmd->child;
                      break;
                  }
             
			  }else if(r==PARTMATCH){
                  nb_match++;
                  if(!matched_cmd){
                      matched_cmd=cm;
                      common_len=strlen((uchar*)cm->name);
                  }else{
                      for(i=_str_len;cm->name[i] && i<common_len &&
                            cm->name[i]==matched_cmd->name[i];i++);
                      if(i<common_len)
                        common_len=i;
                  }
              }
         }
          
		 if(cm)
            continue;
          
		 if(matched_cmd){
              if(_str_len==common_len){
                  puts("\r\n");
                  
				  for(cm=cmd;cm;cm=cm->next){
                      int r=strstart((uchar*)cm->name,__str);
                      if(r==FULLMATCH || r==PARTMATCH){
						  puts("  ");
						  puts(cm->name);
                          puts("\r\n");
                       }
                  }
				  return 1;
             
			  }else{
                  
				  for(i=_str_len;i<common_len;i++)
                    tinysh_char_in(matched_cmd->name[i]);
                  if(nb_match==1)
                    tinysh_char_in(' ');
              }
         }
         return 0;
    
	  }else{ 					// UNMATCH.
         return 0;
      }
   }
}

/************************************************
 * start a new line
 ************************************************/
static void start_of_line()
{
  /* display start of new line */
  puts(prompt);
  if(cur_context){
     puts((char*)context_buffer);
     puts("> ");
  }
  cur_index=0;
}

/****************************************** 
 * character input
 ******************************************/
static void _tinysh_char_in(uchar c)
{
  uchar *line=input_buffers[cur_buf_index];

  if(c=='\n' || c=='\r'){ 					// validate command.
      tinysh_cmd_t *cmd;

	  /* first, echo the newline */
      if(echo)
        putchar(c);

      while(*line && *line==' ') line++;
      
	  if(*line){ 							// not empty line.
          cmd=cur_cmd_ctx?cur_cmd_ctx->child:root_cmd;
          exec_command_line(cmd,line);
          cur_buf_index=(cur_buf_index+1)%HISTORY_DEPTH;
          cur_index=0;
          input_buffers[cur_buf_index][0]=0;
      }
      start_of_line();
  
  }else if(c==TOPCHAR){ 					// return to top level.
	if(echo)
	  putchar(c);

	cur_context=0;
	cur_cmd_ctx=0;
  
  }else if(c ==8 || c ==127){ 				// backspace
    if(cur_index>0){
	   puts("\b \b");
       cur_index--;
       line[cur_index]=0;
    }
  
  }else if(c==16){ 							// CTRL-P: back in history.
      int prevline=(cur_buf_index+HISTORY_DEPTH-1)%HISTORY_DEPTH;

      if(input_buffers[prevline][0]){
          line=input_buffers[prevline];
          
		  /* fill the rest of the line with spaces */
          while(cur_index-->strlen(line))
            puts("\b \b");
          putchar('\r');
          start_of_line();
          puts((char*)line);
          cur_index=strlen(line);
          cur_buf_index=prevline;
      }

  }else if(c==14){ 							// CTRL-N: next in history.

	  int nextline=(cur_buf_index+1)%HISTORY_DEPTH;

	  if(input_buffers[nextline][0])
		{
		  line=input_buffers[nextline];
		  /* fill the rest of the line with spaces */
		  while(cur_index-->strlen(line))
			puts("\b \b");
		  putchar('\r');
		  start_of_line();
		  puts((char*)line);
		  cur_index=strlen(line);
		  cur_buf_index=nextline;
		}

  }else if(c=='?'){ 							// display help.
	  puts("\r\n\n");
	  tinysh_cmd_t *cmd;
      cmd=cur_cmd_ctx?cur_cmd_ctx->child:root_cmd;
      help_command_line(cmd,line);
      start_of_line();
      puts((char*)line);
      cur_index=strlen(line);
    
  }else if(c==9){ 								// TAB: autocompletion.
	  tinysh_cmd_t *cmd;
      cmd=cur_cmd_ctx?cur_cmd_ctx->child:root_cmd;
      
	  if(complete_command_line(cmd,line)){
         start_of_line();
         puts((char*)line);
      }
      cur_index=strlen(line);
  
  }else{ 										// any input character */
    if(cur_index<BUFFER_SIZE){ 
       if(echo)
         putchar(c);
       line[cur_index++]=c;
       line[cur_index]=0;
    }
  }
}

/*******************************************
 * new character input 
 *******************************************/
void tinysh_char_in(uchar c)
{
  /* filter characters here */
  _tinysh_char_in(c);
}

/********************************************
 * add a new command 
 ********************************************/
void tinysh_add_command(tinysh_cmd_t *cmd)
{
  tinysh_cmd_t *cm;

  if(cmd->parent){
      cm=cmd->parent->child;
      if(!cm){
          cmd->parent->child=cmd;
      }else{
          while(cm->next) cm=cm->next;
          cm->next=cmd;
      }
  }else if(!root_cmd){
      root_cmd=cmd;
  }else{
      cm=root_cmd;
      while(cm->next) cm=cm->next;
      cm->next=cmd;
  }
}

/***************************************************
 * modify shell prompt 
 ***************************************************/
void tinysh_set_prompt(char *str)
{
  int i;
  for(i=0;str[i] && i<PROMPT_SIZE;i++)
    prompt[i]=str[i];

  prompt[i]=0;
  /* force prompt display by generating empty command */
  tinysh_char_in('\r');
}

/****************************************************
 * return current command argument
 ****************************************************/
void *tinysh_get_arg()
{
  return tinysh_arg;
}

/****************************************************
 * string to decimal/hexadecimal conversion
 ****************************************************/
unsigned long tinysh_atoxi(char *s)
{
  int ishex=0;
  unsigned long res=0;

  if(*s==0){ 
	return 0;
  }

  if(*s=='0' && *(s+1)=='x'){
    ishex=1;
    s+=2;
  }

  while(*s){
	if(ishex){
		res*=16;
    }else{
		res*=10;
	}

    if(*s>='0' && *s<='9')
		res+=*s-'0';
    else if(ishex && *s>='a' && *s<='f')
		res+=*s+10-'a';
    else if(ishex && *s>='A' && *s<='F')
		res+=*s+10-'A';
    else
		break;

    s++;
  }

  return res;
}





