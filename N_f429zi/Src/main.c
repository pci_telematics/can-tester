
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
  
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_nucleo_144.h"
#include "tm_stm32_usb_device.h"
#include "tm_stm32_usb_device_cdc.h"
#include "tinysh.h"


#define RTC_ASYNCH_PREDIV  0x7F   /* LSE as RTC clock */
#define RTC_SYNCH_PREDIV   0x00FF /* LSE as RTC clock */


#define KEY_PRESSED     	0x01
#define KEY_NOT_PRESSED 	0x00

#define I2CS_ADDRESS        0x3E  	/* Real 7 bits slave address value in Datasheet is: b0011111
                                    mean in uint8_t equivalent at 0x1F and this value can be
                                    seen in the OAR1 register in bits ADD[1:7] */
#define MASTER_REQ_READ    	0x12
#define MASTER_REQ_WRITE   	0x34

/* I2C SPEEDCLOCK define to max value: 400 KHz on STM32F4xx*/
#define I2C_SPEEDCLOCK   	100000
#define I2C_DUTYCYCLE    	I2C_DUTYCYCLE_2


/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan1;
UART_HandleTypeDef huart2;

CAN_TxHeaderTypeDef   TxHeader;
CAN_RxHeaderTypeDef   RxHeader;
uint8_t               TxData[8];
uint8_t               RxData[8];
uint32_t              TxMailbox;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi3;

uint32_t uwPrescalerValue = 0;			// Prescaler declaration 
TIM_HandleTypeDef TimHandle;

RTC_HandleTypeDef RtcHandle;

uint8_t spimTxBuff[SPIBUFFER];
uint8_t spimRxBuff[SPIBUFFER];			// When the CRC feature is enabled the pRxData Length must be Size + 1

uint8_t spisTxBuff[SPIBUFFER];
uint8_t spisRxBuff[SPIBUFFER];

uint8_t spisize = SPIBUFFER;

uint8_t i2cMTxReq[I2CBUFFER];
uint8_t i2cMRxReq[I2CBUFFER];

uint8_t i2cSTxReq[I2CBUFFER];
uint8_t i2cSRxReq[I2CBUFFER];

uint8_t i2cRsize = 5;
uint8_t i2cTsize = 5;

uint16_t i2cAddr = I2CS_ADDRESS;
t_i2cSlaveStat i2cSlaveStat; 

TIM_HandleTypeDef htim6;
ADC_HandleTypeDef AdcHandle;


uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];
uint8_t UART_Buffer[UART_BUFFER_SIZE];

/* USB CDC settings */
TM_USBD_CDC_Settings_t USB_Settings;

uint8_t buttonFlag;
uint8_t buttonPress = 0;

uint8_t timerSet = 5;			//interrupt every 5s.
uint8_t timerFlag;

DMA_HandleTypeDef hdma_usart2_rx;

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI1_Init(void);					//SPI1 Slave.
static void MX_SPI3_Init(void);					//SPI3 Master.
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_TIM3_Init(void);
static void MX_ADC1_Init(void);
static void MX_RTC_Init(void);
static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate);

/* Private function prototypes -----------------------------------------------*/


/*******************************************************
  * @brief  User button operation.
  * @retval None
  ******************************************************/
static void userbutton_test(void)
{  
  uint8_t aShowTime[50] = {0};
  uint8_t aShowDate[50] = {0};
  RTC_CalendarShow(aShowTime, aShowDate);
  
  printf("\r\n%s | %s\r\n\n",aShowDate,aShowTime);
	
  if(buttonFlag & (1 << CAN_REQ)){  
	  uint32_t canid;
	  uint8_t i;
	  
	  printf("CAN FRAME SENT\r\n");  
		
	  if(TxHeader.StdId){
		canid = TxHeader.StdId;
		printf("CAN STD ID = %d | DLC = %d\r\n",canid, TxHeader.DLC);
	  }else{
		canid = TxHeader.ExtId;
		printf("CAN EXT ID = %d | DLC = %d\r\n",canid, TxHeader.DLC);
	  }
	   
	  for(i=0;i<sizeof(TxData);i++){
		printf("CAN TX Data[%d] = %d\r\n",i,TxData[i]);
	  }
	  printf("\r\n\n"); 
		
	  /* Request transmission */
	  if(HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox) != HAL_OK)
	  {
		_Error_Handler(__FILE__, __LINE__);
	  }
  }
  
  if(buttonFlag & (1 << SPIM_REQ)){   
	  memset(spimRxBuff,0,sizeof(spimRxBuff));
	  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_RESET);
	  
	  if(HAL_SPI_TransmitReceive_IT(&hspi3,spimTxBuff,spimRxBuff, spisize) != HAL_OK)
	  {
		 /* Transfer error in transmission process */
		 HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
		 _Error_Handler(__FILE__, __LINE__);
	  }
  }
  
  
  if(buttonFlag & (1 << I2CM_REQ)){   
	  	  
	  if(buttonPress == 0){
		  if(HAL_I2C_Master_Transmit_IT(&hi2c1, (uint16_t)i2cAddr,i2cMTxReq, i2cTsize)!= HAL_OK){
			_Error_Handler(__FILE__, __LINE__);
		  }
	  }else{
		  if(HAL_I2C_Master_Receive_IT(&hi2c1, (uint16_t)i2cAddr,i2cMRxReq, i2cRsize)!= HAL_OK){
			_Error_Handler(__FILE__, __LINE__);
		  }
	  }
  }
  
  if(buttonFlag & (1 << ADC_REQ)){   
	  if (HAL_ADC_Start_IT(&AdcHandle) != HAL_OK)
      {
		_Error_Handler(__FILE__, __LINE__);
      }
  }
  
  if(buttonPress){
	buttonPress = 0;
  }else{
	buttonPress++;  
  }
  
}


/*******************************************************
  * @brief  timer operation.
  * @retval None
  ******************************************************/
void timerSetCallback(void)
{
	switch(timerFlag){   
		case TIMER_BUTTON:  
			userbutton_test();
			break;
		case TIMER_CUSTOM:
			//Put custom function here
			break;
		default:
			break;
	}
}



/*******************************************************
  * @brief  The application entry point.
  * @retval None
  ******************************************************/
int main(void)
{
  char inputchar;
	
  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  
  BSP_LED_Init(LED1);
  BSP_LED_Init(LED2);
  BSP_LED_Init(LED3);
  
  MX_CAN1_Init(LOOPBACK_250);
	
  MX_SPI1_Init();
  MX_SPI3_Init();
  
  MX_I2C1_Init();
  MX_I2C2_Init();
  
  BSP_PB_Init(BUTTON_USER, BUTTON_MODE_GPIO);
  
  /* Init USB peripheral */
  TM_USB_Init();
	
  /* Init VCP on FS port */
  TM_USBD_CDC_Init(TM_USB_FS);
	
  /* Start USB device mode on FS port */
  TM_USBD_Start(TM_USB_FS);
  
  //buttonFlag |= 1UL << CAN_REQ;  			//Turning ON CAN.
  buttonFlag &= ~(1UL << CAN_REQ);  		//Turning OFF CAN.
  
  //buttonFlag |= 1UL << SPIM_REQ;  		//Turning ON SPIM.
  buttonFlag &= ~(1UL << SPIM_REQ);			//Turning OFF SPIM.
  
  buttonFlag |= 1UL << I2CM_REQ;  			//Turning ON I2CM.
  //buttonFlag &= ~(1UL << I2CM_REQ);		//Turning OFF I2CM.
  
  /* SPI3 TX SLAVE Default Setup */
  spimTxBuff[0] = 10;
  spimTxBuff[1] = 15;
  spimTxBuff[2] = 20;
  spimTxBuff[3] = 25;
  spimTxBuff[4] = 30;
  
  /* SPI1 TX SLAVE Default Setup */
  spisTxBuff[0] = 40;
  spisTxBuff[1] = 50;
  spisTxBuff[2] = 60;
  spisTxBuff[3] = 70;
  spisTxBuff[4] = 80;
  
  /* I2C1 TX MASTER Default Setup */
  i2cMTxReq[0] = 5;
  i2cMTxReq[1] = 7;
  i2cMTxReq[2] = 9;
  i2cMTxReq[3] = 11;
  i2cMTxReq[4] = 13;
  
  /* I2C2 TX MASTER Default Setup */
  i2cSTxReq[0] = 20;
  i2cSTxReq[1] = 21;
  i2cSTxReq[2] = 22;
  i2cSTxReq[3] = 23;
  i2cSTxReq[4] = 24;
  
  /* SPI Slave receive request from master */
  if(HAL_SPI_TransmitReceive_IT(&hspi1, spisTxBuff, spisRxBuff, spisize) != HAL_OK)	
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
  /* I2C Slave setup from master */
  if(buttonPress == 0){
	  i2cSlaveStat = I2CSLAVE_WAIT_RCV;
	  if(HAL_I2C_Slave_Receive_DMA(&hi2c2,i2cSRxReq, i2cTsize)!= HAL_OK)
	  {
		_Error_Handler(__FILE__, __LINE__);
	  }
  }else{
	  i2cSlaveStat = I2CSLAVE_WAIT_TMT;
	  if(HAL_I2C_Slave_Transmit_IT(&hi2c2,i2cSTxReq, i2cRsize)!= HAL_OK)
	  {
		_Error_Handler(__FILE__, __LINE__);
	  }
  }	  
  
  MX_TIM3_Init();
  MX_ADC1_Init();
  MX_RTC_Init();
 
  /* Infinite loop */
  while (1)
  {    
	if(BSP_PB_GetState(BUTTON_USER) == KEY_PRESSED){
		userbutton_test();
		
		while(BSP_PB_GetState(BUTTON_USER) == KEY_PRESSED){
			HAL_Delay(50);
		}
		
	}else{
		BSP_LED_Off(LED2);
	}
	
	TM_USBD_CDC_Process(TM_USB_FS);
		
	/* Check if device is ready, if drivers are installed if needed on FS port */
	if (TM_USBD_IsDeviceReady(TM_USB_FS) == TM_USBD_Result_Ok) {
		
		/* Check if user has changed parameters for COM port */
		TM_USBD_CDC_GetSettings(TM_USB_FS, &USB_Settings);
	
		/* Check if settings updated from user terminal */
		if (USB_Settings.Updated) {
			TM_USBD_CDC_Puts(TM_USB_FS, "PD2  -> SPIM_CS	| PC10 -> SPIM_SCK  | PC11 -> SPIM_MISO  | PC12 -> SPIM_MOSI\r\n");
			TM_USBD_CDC_Puts(TM_USB_FS, "PA4  -> SPIS_CS	| PA5  -> SPIS_SCK  | PA6  -> SPIS_MISO  | PA7  -> SPIS_MOSI\r\n\n"); 
			TM_USBD_CDC_Puts(TM_USB_FS, "PB8  -> I2CM_SCL 	| PB9  -> I2CM_SDA \r\n");
			TM_USBD_CDC_Puts(TM_USB_FS, "PB10 -> I2CS_SCL 	| PB11 -> I2CS_SDA (0x3E)\r\n");
			TM_USBD_CDC_Puts(TM_USB_FS, "PD5  -> UART2_TX 	| PD6  -> UART2_RX \r\n");
			TM_USBD_CDC_Puts(TM_USB_FS, "PD0  -> CAN1_RX 	| PD1  -> CAN1_TX \r\n");
			TM_USBD_CDC_Puts(TM_USB_FS, "PC0  -> ADC1\r\n");
			TM_USBD_CDC_Puts(TM_USB_FS, "\r\n");
			
			/* Shell Terminal */
			tinysh_init();
		}
		
		/* Check if One character received received on FS port */
		if (TM_USBD_CDC_Getc(TM_USB_FS, &inputchar)) {
			
			/* Send it back */
			//TM_USBD_CDC_Putc(TM_USB_FS, inputchar);
			tinysh_char_in((unsigned char)inputchar);
		}
	}
	
	switch (i2cSlaveStat)
    {
		case I2CSLAVE_RCV_COMPLETE:
			i2cSlaveStat = I2CSLAVE_WAIT_TMT;
			if(HAL_I2C_Slave_Transmit_IT(&hi2c2,i2cSTxReq, i2cRsize)!= HAL_OK)
			{
				_Error_Handler(__FILE__, __LINE__);
			}
			break;
		
		case I2CSLAVE_TMT_COMPLETE:
			i2cSlaveStat = I2CSLAVE_WAIT_RCV;
		    if(HAL_I2C_Slave_Receive_DMA(&hi2c2,i2cSRxReq, i2cTsize)!= HAL_OK){
				_Error_Handler(__FILE__, __LINE__);
			}
			break;
			
		default:
			break;
	}
	
	HAL_Delay(100);
	
  }
}


/******************************************************************************
  * @brief  Rx Fifo 0 message pending callback
  * @param  hcan: pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @retval None
  *****************************************************************************/
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
  printf("CAN FRAME RECEIVED\r\n");
	
  /* Get RX message */
  if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData) != HAL_OK)
  {
    /* Reception Error */
    _Error_Handler(__FILE__, __LINE__);
  }
	
  BSP_LED_On(LED2);
 
  if(RxHeader.RTR == CAN_RTR_DATA){
	printf("CAN RTR : DATA FRAME\r\n");
  }else{
	printf("CAN RTR : REMOTE FRAME\r\n");
  }
  
  if(RxHeader.IDE == CAN_ID_STD){
	printf("CAN ID STD = %d | CAN DLC = %d\r\n",RxHeader.StdId,RxHeader.DLC);
  }else{
	printf("CAN ID EXT = %d | CAN DLC = %d\r\n",RxHeader.ExtId,RxHeader.DLC);
  }
  
  uint8_t i;
  for(i=0;i<sizeof(RxData);i++){
	printf("CAN RCV Data[%d] = %d\r\n",i,RxData[i]);
  }
  
  printf("\r\n\n");
}


/*********************************************************
 * @brief  SPI TxRx Transfer completed callback.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	uint8_t i;
	
	if(hspi->Instance == SPI3){
    	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
		
		for(i=0;i<spisize;i++){
			printf("SPI3 RX [%d] = %d\r\n",i,spimRxBuff[i]);
		}
		
    }else if(hspi->Instance == SPI1){
    	BSP_LED_Toggle(LED1);
		
		for(i=0;i<sizeof(spisRxBuff);i++){
			printf("SPI1 RX [%d] = %d\r\n",i,spisRxBuff[i]);
		}
			  
		if(HAL_SPI_TransmitReceive_IT(&hspi1, (uint8_t*)spisTxBuff, (uint8_t *)spisRxBuff, spisize) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}
    }
	
	printf("\r\n\n");
}

/*********************************************************
 * @brief  SPI Rx Transfer completed callback.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi->Instance == SPI1){
    	BSP_LED_Toggle(LED1);
		printf("\r\nSPI1 SLAVE RX:\r\n");
		
		uint8_t i;
		for(i=0;i<spisize;i++){
			printf("SPI1 RX data [%d] = %d\r\n",i,spisRxBuff[i]);
		}
			  
		if(HAL_SPI_Receive_IT(&hspi1,spisRxBuff, spisize) != HAL_OK){
			_Error_Handler(__FILE__, __LINE__);
		}
	}
}

/*********************************************************
 * @brief  SPI Rx Transfer completed callback.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi->Instance == SPI1){
		printf("\r\nSPI1 TX callback\r\n");	
	}
	else if(hspi->Instance == SPI3){
		printf("\r\nSPI3 TX callback\r\n");	
	}
}

/*********************************************************
 * @brief  SPI Error callback.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
	_Error_Handler(__FILE__, __LINE__);
}

/*********************************************************
 * @brief  SPI Abort callback.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_SPI_AbortCpltCallback(SPI_HandleTypeDef *hspi)
{
	printf("\r\nSPI Abort Complete\r\n");
}


/*********************************************************
 * @brief  I2C1 MASTER TX CB.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C1){
		//printf("I2C1 TX CB\r\n");
	}
}

/*********************************************************
 * @brief  I2C1 MASTER RX CB.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	uint8_t i;
	
	if(hi2c->Instance == I2C1){
		for(i=0;i<i2cRsize;i++){
			printf("I2C1 Master RX[%d]: %d\r\n",i,i2cMRxReq[i]);
		}
	}
}

/*********************************************************
 * @brief  I2C2 Slave RX CB.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	uint8_t i;
	
	if(hi2c->Instance == I2C2){
		for(i=0;i<i2cTsize;i++){
			printf("I2C2 Slave RX[%d]: %d\r\n",i,i2cSRxReq[i]);
		}
		i2cSlaveStat = I2CSLAVE_RCV_COMPLETE;
	}
}

/*********************************************************
 * @brief  I2C2 Slave TX CB.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	if(hi2c->Instance == I2C2)
	{
	  //printf("I2C2 TX CB\r\n");
	  i2cSlaveStat = I2CSLAVE_TMT_COMPLETE;
	}
}

/*********************************************************
 * @brief  I2C Error callback.
 * @param  hspi: SPI handle
 * @retval None
 ********************************************************/
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *I2cHandle)
{
  /** Error_Handler() function is called when error occurs.
  * 1- When Slave don't acknowledge it's address, Master restarts communication.
  * 2- When Master don't acknowledge the last data transferred, Slave don't care in this example.
  */
  printf("HAL_I2C_ErrorCB\r\n");
  if (HAL_I2C_GetError(I2cHandle) != HAL_I2C_ERROR_AF)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}






/**************************************************************
  * @brief System Clock Configuration
  * @retval None
  *************************************************************/
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  /**Configure the main internal regulator output voltage */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /**Initializes the CPU, AHB and APB busses clocks */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  /**Initializes the CPU, AHB and APB busses clocks */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  /**Configure the Systick interrupt time */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  /**Configure the Systick */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
  
}

/*********************************************************
 * @brief  CAN init Function.
 * @param  canset: CAN settings
 * @retval None
 ********************************************************/
void MX_CAN1_Init(t_canInitSetting canset)
{
  hcan1.Instance = CAN1;
	
  if((canset == NORMAL_250)||(canset == LOOPBACK_500)){
	/* 42(APB1)/(prescaler*(CAN_SJW_1TQ+CAN_BS1_8TQ+CAN_BS2_5TQ) = 250kbps */
	hcan1.Init.Prescaler = 12;
  }else{
	/* 42(APB1)/(prescaler*(CAN_SJW_1TQ+CAN_BS1_8TQ+CAN_BS2_5TQ) = 500kbps */
	hcan1.Init.Prescaler = 6;
  }
  
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_8TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_5TQ;
  
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
	
  /* Change to use single board or pair board */	
  if((canset == NORMAL_250)||(canset == NORMAL_500)){
	hcan1.Init.Mode = CAN_MODE_NORMAL;
  }else{
	hcan1.Init.Mode = CAN_MODE_LOOPBACK;	
  }
	
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  CAN_FilterTypeDef  sFilterConfig;
  sFilterConfig.FilterBank = 0;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = 0x0000;
  sFilterConfig.FilterIdLow = 0x0000;
  sFilterConfig.FilterMaskIdHigh = 0x0000;
  sFilterConfig.FilterMaskIdLow = 0x0000;
  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.SlaveStartFilterBank = 14;
  
  if(HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK)
  {
    /* Filter configuration Error */
    _Error_Handler(__FILE__, __LINE__);;
  }
  
  if (HAL_CAN_Start(&hcan1) != HAL_OK)
  {
    /* Start Error */
    _Error_Handler(__FILE__, __LINE__);;
  }
  
  if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
  {
    /* Notification Error */
    _Error_Handler(__FILE__, __LINE__);;
  }
  
  //TxHeader.StdId = 0x09;
  ///TxHeader.IDE = CAN_ID_STD;
  
  TxHeader.ExtId = 0x09;
  TxHeader.IDE = CAN_ID_EXT;
  
  TxHeader.RTR = CAN_RTR_DATA;
  TxHeader.TransmitGlobalTime = DISABLE;
  
  TxHeader.DLC = 2;
  TxData[0] = 0xCA;
  TxData[1] = 0xFE;
}

/*************************************
 * @brief USART2 init function 
 *************************************/
static void MX_USART2_UART_Init(void)
{
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  __HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);   		// enable idle line interrupt
  __HAL_DMA_ENABLE_IT (&hdma_usart2_rx, DMA_IT_TC);  	// enable DMA Tx cplt interrupt
  
  HAL_UART_Receive_DMA (&huart2, DMA_RX_Buffer, DMA_RX_BUFFER_SIZE);
  hdma_usart2_rx.Instance->CR &= ~DMA_SxCR_HTIE;  		// disable uart half tx interrupt
  
  
  uint8_t DEBUGMSG[20] = "UART2 READY\n\r";
  HAL_UART_Transmit(&huart2, DEBUGMSG, sizeof(DEBUGMSG), 10);
}



/**********************************************************************
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  *********************************************************************/
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART3 and Loop until the end of transmission */
  //HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);
  TM_USBD_CDC_Putc(TM_USB_FS,ch);
  return ch;
}


/*******************************************
 * @brief I2C1 init function as MASTER
 *******************************************/
static void MX_I2C1_Init(void)
{
  hi2c1.Instance 				= I2C1;
  hi2c1.Init.ClockSpeed 		= I2C_SPEEDCLOCK;;
  hi2c1.Init.DutyCycle 			= I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 		= 0;
  hi2c1.Init.AddressingMode 	= I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode 	= I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 		= 0;
  hi2c1.Init.GeneralCallMode 	= I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode 		= I2C_NOSTRETCH_DISABLE;
  
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/*******************************************
 * @brief I2C2 init function as SLAVE
 *******************************************/
static void MX_I2C2_Init(void)
{
  hi2c2.Instance 				= I2C2;
  hi2c2.Init.ClockSpeed 		= I2C_SPEEDCLOCK;
  hi2c2.Init.DutyCycle 			= I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 		= I2CS_ADDRESS;
  hi2c2.Init.AddressingMode 	= I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode 	= I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 		= 0;
  hi2c2.Init.GeneralCallMode 	= I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode 		= I2C_NOSTRETCH_DISABLE;
  
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
}

/*******************************************
 * @brief Set I2C1 TX data
 *******************************************/
uint8_t i2c1_transmit(uint8_t *tx_data, uint8_t count,uint16_t addr)
{
	HAL_StatusTypeDef status = HAL_OK;
	
	i2cAddr = addr;
	memcpy(i2cMTxReq,tx_data,count);
	i2cTsize = count;
    return status;
}


/*******************************************
 * @brief Set I2C2 TX data
 *******************************************/
uint8_t i2c2_transmit(uint8_t *tx_data,uint8_t count)
{
	HAL_StatusTypeDef status = HAL_OK;
	
	memcpy(i2cSTxReq,tx_data,count);
	i2cRsize = count;
    return status;
}



/*******************************************
 * @brief SPI3 init function as MASTER
 *******************************************/
static void MX_SPI3_Init(void)
{
  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 10;
	
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
 
	
  GPIO_InitTypeDef gpio_init;

  __HAL_RCC_GPIOD_CLK_ENABLE();

  gpio_init.Pin 	= GPIO_PIN_2;
  gpio_init.Mode 	= GPIO_MODE_OUTPUT_PP;
  gpio_init.Pull 	= GPIO_PULLUP;
  gpio_init.Speed   = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOD, &gpio_init);

  /* Initial state for CS */ 
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
}

/*******************************************
 * @brief SPI1 init function as SLAVE
 *******************************************/
static void MX_SPI1_Init(void)
{
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  //hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi1.Init.Mode = SPI_MODE_SLAVE;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_HARD_INPUT;
  //hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  
	
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/*******************************************
 * @brief Set SPI3 TX data
 *******************************************/
uint8_t spi3_transmit_receive(uint8_t *tx_data, uint16_t count)
{
	HAL_StatusTypeDef status = HAL_OK;
	
	if(HAL_SPI_GetState(&hspi3) != HAL_SPI_STATE_READY){
		printf("SPI3 Busy\r\n");
		status = HAL_ERROR;
		return status;
	}
	
	uint8_t i;
	for(i=0;i<count;i++){
		spimTxBuff[i] = *(tx_data+i);
	}
	
    return status;
}


/*******************************************
 * @brief Set SPI1 TX data
 *******************************************/
uint8_t spi1_transmit_receive(uint8_t *tx_data, uint16_t count)
{
	HAL_StatusTypeDef status = HAL_OK;
	
	memcpy(spisTxBuff,tx_data,count);
	
#if 0	
	if(HAL_SPI_Abort_IT(&hspi1)!= HAL_OK){
		_Error_Handler(__FILE__, __LINE__);
	}
	
	__HAL_SPI_CLEAR_OVRFLAG(&hspi1);	
	
	if(HAL_SPI_TransmitReceive_IT(&hspi1, (uint8_t*)spisTxBuff, (uint8_t *)spisRxBuff, spisize) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
#endif	

    return status;
}


/*************************************************************************************************
 * @brief TIMER 3 INIT
   Update rate = TIM3 counter clock / (Period + 1) = 1 Hz
   SystemCoreClock is set to 168 MHz, PCLK1 = 42 MHz (with prescaler 4).
   TIM3 input clock (TIM3CLK) = APB1 clock (PCLK1) x2.
   Thus TIM3CLK = PCLK1*2 = HCLK/2 = SystemCoreClock/2

   To get TIM3 counter clock at 10 KHz, the Prescaler is computed as follows:
   Prescaler = (TIM3CLK / TIM3 counter clock) - 1
   Prescaler = ((SystemCoreClock/2) /10 KHz) - 1
 **************************************************************************************************/
static void MX_TIM3_Init(void)
{
   /* Compute the prescaler value to have TIMx counter clock equal to 10000 Hz */
   uwPrescalerValue = (uint32_t)((SystemCoreClock / 2) / 10000) - 1;
	
   //printf("core clock: %d, prescaler: %d\r\n",SystemCoreClock,uwPrescalerValue);
	
   TimHandle.Instance 				= TIM3;
   TimHandle.Init.Period            = 10000 - 1;
   TimHandle.Init.Prescaler         = uwPrescalerValue;
   TimHandle.Init.ClockDivision     = 0;
   TimHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
   TimHandle.Init.RepetitionCounter = 0;
	
   if (HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
   {
		_Error_Handler(__FILE__, __LINE__);
   }
   
   if (HAL_TIM_Base_Start_IT(&TimHandle) != HAL_OK)
   {
		_Error_Handler(__FILE__, __LINE__);
   }
}


/************************************************************
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim : TIM handle
  * @retval None
  ***********************************************************/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
   static uint8_t timcount;
	
   if(timcount > timerSet){
	   BSP_LED_On(LED3);
	   timcount = 0;
	   timerSetCallback();
   }else{
	   timcount++;
	   BSP_LED_Off(LED3);
   }
}


/***************************************************************
  * @brief  Conversion complete callback in non blocking mode
  * @param  AdcHandle : AdcHandle handle
  * @retval None
  **************************************************************/
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{
  uint16_t uhADCxConvertedValue;
	
  uhADCxConvertedValue = HAL_ADC_GetValue(AdcHandle);
  printf("ADC1 : %d \r\n",uhADCxConvertedValue);
}


/***************************************************************
  * @brief  ADC1 Initialization
  **************************************************************/
static void MX_ADC1_Init(void)
{
	ADC_ChannelConfTypeDef sConfig;
	
	AdcHandle.Instance                   = ADC1;
    AdcHandle.Init.ClockPrescaler        = ADC_CLOCKPRESCALER_PCLK_DIV4;
    AdcHandle.Init.Resolution            = ADC_RESOLUTION_12B;
    AdcHandle.Init.ScanConvMode          = DISABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
    AdcHandle.Init.ContinuousConvMode    = DISABLE;                       /* Continuous mode disabled to have only 1 conversion at each conversion trig */
    AdcHandle.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
    AdcHandle.Init.NbrOfDiscConversion   = 0;
    AdcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE; /* Conversion start trigged at each external event */
    AdcHandle.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_T1_CC1;
    AdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    AdcHandle.Init.NbrOfConversion       = 1;
    AdcHandle.Init.DMAContinuousRequests = DISABLE;
    AdcHandle.Init.EOCSelection          = DISABLE;

    if (HAL_ADC_Init(&AdcHandle) != HAL_OK)
    {
		_Error_Handler(__FILE__, __LINE__);
    }
	
	sConfig.Channel      = ADC_CHANNEL_10;
    sConfig.Rank         = 1;
    sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
    sConfig.Offset       = 0;
    if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
    {
		_Error_Handler(__FILE__, __LINE__);
    }
}


/*******************************************************
  * @brief  Configure the current time and date.
  * @param  None
  * @retval None
  ******************************************************/
void RTC_CalendarConfig(RTC_DateTypeDef *date,RTC_TimeTypeDef *time)
{
  RTC_DateTypeDef sdatestructure;
  RTC_TimeTypeDef stimestructure;
	
  if(date == NULL){	
	/* Set Date: Friday December 18th 2018 */
	sdatestructure.Year 		= 0x18;
	sdatestructure.Month 		= RTC_MONTH_DECEMBER;
	sdatestructure.Date 		= 0x14;
	sdatestructure.WeekDay 		= RTC_WEEKDAY_FRIDAY;
  }else{
	sdatestructure = *date;
  }
  
  if(HAL_RTC_SetDate(&RtcHandle,&sdatestructure,RTC_FORMAT_BCD) != HAL_OK)
  {
	_Error_Handler(__FILE__, __LINE__);
  }
	
  if(time == NULL){	
	/* Set Time: 08:00:00 */
	stimestructure.Hours 	= 0x08;
	stimestructure.Minutes 	= 0x00;
	stimestructure.Seconds 	= 0x00;
	stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
	stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;
  }else{
	stimestructure = *time;
  }

  if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  /* Writes a data in a RTC Backup data Register1 */
  HAL_RTCEx_BKUPWrite(&RtcHandle, RTC_BKP_DR1, 0x32F2);
}


/***************************************************************
  * @brief  RTC Initialization
  **************************************************************/
static void MX_RTC_Init(void)
{
  /* RTC configured as follows:
  - Hour Format    = Format 24
  - Asynch Prediv  = Value according to source clock
  - Synch Prediv   = Value according to source clock
  - OutPut         = Output Disable
  - OutPutPolarity = High Polarity
  - OutPutType     = Open Drain */ 
  RtcHandle.Instance 			= RTC; 
  RtcHandle.Init.HourFormat 	= RTC_HOURFORMAT_24;
  RtcHandle.Init.AsynchPrediv 	= RTC_ASYNCH_PREDIV;
  RtcHandle.Init.SynchPrediv 	= RTC_SYNCH_PREDIV;
  RtcHandle.Init.OutPut 		= RTC_OUTPUT_DISABLE;
  RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  RtcHandle.Init.OutPutType 	= RTC_OUTPUT_TYPE_OPENDRAIN;
  __HAL_RTC_RESET_HANDLE_STATE(&RtcHandle);
	
  if (HAL_RTC_Init(&RtcHandle) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
 
  if (HAL_RTCEx_BKUPRead(&RtcHandle, RTC_BKP_DR1) != 0x32F2)	// Read the Back Up Register 1 Data
  {
    RTC_CalendarConfig(NULL,NULL);								// Configure RTC Calendar.
  }else{
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST) != RESET)	 		// Check if the Power On Reset flag is set.
    {
      //printf("Power on reset\r\n");						
    }
    
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST) != RESET)			// Check if Pin Reset flag is set.
    {
      //printf("External reset\r\n");	
    }
    __HAL_RCC_CLEAR_RESET_FLAGS();								// Clear source Reset Flag.
  }
}


/*******************************************************
  * @brief  Display the current time and date.
  * @param  showtime : pointer to buffer
  * @param  showdate : pointer to buffer
  * @retval None
  ******************************************************/
static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate)
{
  RTC_DateTypeDef sdatestructureget;
  RTC_TimeTypeDef stimestructureget;

  /* Get the RTC current Time */
  HAL_RTC_GetTime(&RtcHandle, &stimestructureget, RTC_FORMAT_BIN);
  /* Get the RTC current Date */
  HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, RTC_FORMAT_BIN);
  /* Display time Format : hh:mm:ss */
  sprintf((char *)showtime, "%2d:%2d:%2d", stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
  /* Display date Format : mm-dd-yy */
  sprintf((char *)showdate, "%2d-%2d-%2d", sdatestructureget.Month, sdatestructureget.Date, 2000 + sdatestructureget.Year);
}


/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_PowerSwitchOn_GPIO_Port, USB_PowerSwitchOn_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD1_GPIO_Port, LD1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : USER_Btn_Pin */
  GPIO_InitStruct.Pin = USER_Btn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USER_Btn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD3_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LD3_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = USB_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OverCurrent_Pin */
  GPIO_InitStruct.Pin = USB_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD1_Pin */
  GPIO_InitStruct.Pin = LD1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD1_GPIO_Port, &GPIO_InitStruct);

}

/****************************************************************************
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  ***************************************************************************/
void _Error_Handler(char *file, int line)
{
  printf("ER: %s line %d\r\n", file, line);
  BSP_LED_On(LED3);
	
  while(1)
  {
	BSP_LED_Toggle(LED3);
	HAL_Delay(100);
  }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
}
#endif /* USE_FULL_ASSERT */



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
